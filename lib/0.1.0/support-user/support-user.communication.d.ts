import { CommunicationDataType, CommunicationTypeEnum } from "@timerocket/assistant-data-model";
export interface SupportUserCommunication {
    fromUser: string;
    toUser: string;
    fromDevice: string;
    toDevice: string;
    id: string;
    text: string;
    type: CommunicationTypeEnum;
    createdAt: string;
    data?: CommunicationDataType;
}
