"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupportUserMetaTypeEnum = void 0;
var SupportUserMetaTypeEnum;
(function (SupportUserMetaTypeEnum) {
    SupportUserMetaTypeEnum["SUPPORT_USER_INTERACTION"] = "support-user.interaction";
})(SupportUserMetaTypeEnum = exports.SupportUserMetaTypeEnum || (exports.SupportUserMetaTypeEnum = {}));
