#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const glyphMap = require('./icofont-glyphMap.json');

const lines = [];
lines.push('export enum AppIconEnum {')
for(const key in glyphMap){
    let enumKey = key.toUpperCase().replace(/-/g, '_');
    if(!isNaN(enumKey.substr(0, 1))){
        enumKey = `_${enumKey}`
    }
    lines.push(`  ${enumKey} = "${key}",`);
}
lines.push('}');
lines.push('');

const rootDir = path.dirname(__dirname);
const filePath = `${rootDir}/src/app/app-icon.enum.ts`
fs.writeFileSync(filePath, lines.join('\n'))